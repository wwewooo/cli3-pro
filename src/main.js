import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import http from "./http";
import Vant from 'vant';
import 'vant/lib/index.css';
import {
  Popup 
} from 'vant';

Vue.use(Popup);

Vue.use(Vant);

Vue.config.productionTip = false;
Vue.prototype.$http = http;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");