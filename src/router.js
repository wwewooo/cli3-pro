import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/imgload",
      name: "imgload",
      component: () =>
        import("./views/imgload.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import("./views/sign/login.vue")
    },
    {
      path: "/chat/index",
      name: "chatindex",
      component: () =>
        import("./views/chat/index.vue")
    }
  ]
});
