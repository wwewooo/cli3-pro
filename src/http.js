import axios from "axios";
axios.defaults.baseURL = `${process.env.VUE_APP_API_HOST}/api/v2`;

//添加请求拦截器
axios.interceptors.request.use(config => {
    return config;
}, error => {
    return Promise.reject(error);
});

//添加响应拦截器
axios.interceptors.response.use(response => {
    return response;
}, error => {
    return Promise.reject(error)
})

export default axios;