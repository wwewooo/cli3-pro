module.exports = {
  baseUrl: process.env.BASE_URL || "/",
  lintOnSave: false,
  productionSourceMap: false,
  css: {
    loaderOptions: {},
  },
  devServer: {
    open: false,
    disableHostCheck: true,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_API_HOST,
        changeOrigin: true
      }
    }
  }
}